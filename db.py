from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


# """
# Connexion à la BDD
# """

import types
import contextlib
import mysql.connector

@contextlib.contextmanager
def creer_connexion():
    '''Pour créer une connexion à la BD'''
    conn = mysql.connector.connect(
        user="sammy",
        password="password",
        host="127.0.0.1",
        database="MeTube",
        raise_on_warnings=True
    )

    # Pour ajouter la méthode getCurseur() à l'objet connexion
    conn.get_curseur = types.MethodType(get_curseur, conn)

    try:
        yield conn
    except Exception:
        conn.rollback()
        raise
    else:
        conn.commit()
    finally:
        conn.close()

@contextlib.contextmanager
def get_curseur(self):
    '''Permet d'avoir les enregistrements sous forme de dictionnaires'''
    curseur = self.cursor(dictionary=True)
    try:
        yield curseur
    finally:
        curseur.close()

def authentifier(conn, courriel, mdp):
    """Retourne un utilisateur avec le courriel et le mdp"""
    with conn.get_curseur() as curseur:
        curseur.execute(
            "SELECT * FROM compte_utilisateur WHERE courriel=%(courriel)s and mot_de_passe=%(mdp)s",
            {
                "courriel": courriel,
                "mot_de_passe": mdp
            }
        )
        return curseur.fetchone()

def get_utilisateur(conn, courriel):
    """Retourne un utilisateur"""
    with conn.get_curseur() as curseur:
        curseur.execute(
            "SELECT * FROM compte_utilisateur WHERE courriel=%(courriel)s",
            {
                "courriel": courriel
            }
        )
        return curseur.fetchone()

def get_utilisateurs(conn):
    "Retourne tous les utilisateurs"
    with conn.get_curseur() as curseur:
        curseur.execute("SELECT * FROM compte_utilisateur")
        return curseur.fetchall()

def ajouter_utilisateur(conn, utilisateur):
    "Ajoute un utilisateur dans la BD"
    with conn.get_curseur() as curseur:
        curseur.execute(
            "INSERT INTO compte_utilisateur (courriel, mot_de_passe, admin) VALUES (%(courriel)s, %(mot_de_passe)s, %(admin)s)",
            utilisateur
        )
        return curseur.lastrowid

def retirer_utilisateur(conn, courriel):
    with conn.get_curseur() as curseur:
        curseur.execute(
            "DELETE FROM compte_utilisateur WHERE courriel = %(courriel)s",
            {
                "courriel": courriel
            }
        )
        curseur.close()

def trouverTitres(conn, mot_cle):
    with conn.get_curseur() as curseur:
        curseur.execute("SELECT titre FROM videos WHERE titre LIKE %(mot_cle)s",
        {
            'mot_cle' : f"%{mot_cle}%",
        }
        )
        return curseur.fetchall()

