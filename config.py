class Config(object):
    UPLOAD_FOLDER = "static/video/"
    MAX_CONTENT_LENGTH = 16 * 1000 * 1000  # 16Mo
    UPLOAD_EXTENSIONS = [
        '.mp4', '.mov', '.wmv', '.mpg', '.webm', '.mkv', '.swf']
    SESSION_COOKIE_SECURE = True
    DB_SERVEUR = "localhost"
    DB_NAME = "MeTube"
    DB_USERNAME = "sammy"
    DB_PASSWORD = "password"

class ProductionConfig(Config):
    pass

class DevelopmentConfig(Config):
    DEBUG = True
    DB_NAME = "MeTube"
    DB_USERNAME = "sammy" 
    DB_PASSWORD = "password"
    UPLOAD_FOLDER = "static/video/"
    MAX_CONTENT_LENGTH = 16 * 1000 * 1000  # 16Mo
    UPLOAD_EXTENSIONS = [
        '.mp4', '.mov', '.wmv', '.mpg', '.webm', '.mkv', '.swf']
    SESSION_COOKIE_SECURE = True
    DB_SERVEUR = "localhost"
    DB_NAME = "MeTube"
    DB_USERNAME = "sammy"
    DB_PASSWORD = "password"
    SECRET_KEY = "B\xb2?.\xdf\x9f\xa7m\xf8\x8a%,\xf7\xc4\xfa\x91"
# class Config(object):
#     DEBUG = False
#     TESTING = False

