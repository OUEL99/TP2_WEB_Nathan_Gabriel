from flask import Flask, url_for, render_template, session, jsonify, request
from werkzeug.utils import redirect
from blueprints.etiquettes.etiquettes import etiquette
from blueprints.ajouter.ajouter import ajouter
from blueprints.rechercher.rechercher import rechercher
from blueprints.video.video import video
from blueprints.compte.compte import compte
from db import creer_connexion, trouverTitres
from utilitaires import get_etiquettes
import config


def creer_application():
    app_flask = Flask(__name__)
    app_flask.config.from_object(config.DevelopmentConfig)
    compte_util  = f'{app_flask.config["DB_USERNAME"]}:{app_flask.config["DB_PASSWORD"]}'
    serveur_bd = f'{app_flask.config["DB_SERVEUR"]}/{app_flask.config["DB_NAME"]}'
    app_flask.config['SQLALCHEMY_DATABASE_URI'] = f"""mysql://{compte_util}@{serveur_bd}""" 
    app_flask.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app_flask.register_blueprint(compte)
    app_flask.register_blueprint(ajouter)
    app_flask.register_blueprint(rechercher)
    app_flask.register_blueprint(video)
    app_flask.register_blueprint(etiquette)
    #session["utilisateur"]
    return app_flask


app = creer_application()


@app.route("/", methods=['GET', 'POST'])
def accueil():
    """Returns videos"""

    videos = None
    srcs = [list]
    etiquettes = []
    with creer_connexion() as connexion:
        with connexion.get_curseur() as curseur:
            curseur.execute(
                "select id, titre, description, nom_fichier from videos WHERE visibilite = 0 LIMIT 3")
            videos = curseur.fetchall()
    for le_video in videos:
        srcs.append(app.config['UPLOAD_FOLDER'] + le_video['nom_fichier'])
        if (get_etiquettes(le_video['id']) is not None or "" or []):
            etiquettes.append(get_etiquettes(le_video['id']))
    return render_template("/index.jinja", videos=videos, srcs=srcs, etiquettes=etiquettes, id=id)

@app.route("/api/updateVideos")
def MaJ():
    """Returns videos"""
    videos = None
    with creer_connexion() as connexion:
        with connexion.get_curseur() as curseur:
            curseur.execute(
                "select id, titre, description, nom_fichier from videos WHERE visibilite = 0 LIMIT 3")
            videos = curseur.fetchall()
    return jsonify(videos)

@app.route("/api/etiquettesupdate")
def updateEtiquettes(le_video):
    etiquettes = []
    if (get_etiquettes(le_video['id']) is not None or "" or []):
        etiquettes.append(get_etiquettes(le_video['id']))
    return jsonify(etiquettes)

@app.route("/api/srcsUpdate")
def updateSrcs(le_video):
    srcs = [list]
    srcs.append(app.config['UPLOAD_FOLDER'] + le_video['nom_fichier'])
    return jsonify(srcs)

# @app.route("/api/updateAccueil")
#def update(infosMaJ):
#    return
@app.route("/api/autocompletion")
def autocompletion():
    mot_cles = request.args.get("mots-cles", default="")
    """Recupere les titres des videos pour l'autocompletion"""
    with creer_connexion() as conn:
        resultats = trouverTitres(conn, mot_cles)
    return jsonify(resultats)

@app.route("/api/courriels")
def courriels():
    with creer_connexion() as conn:
         with conn.get_curseur() as curseur:
            curseur.execute(
                "select courriel from compte_utilisateur")
            resultats = curseur.fetchall()
    return jsonify(resultats)

if __name__ == '__main__':
    app.run(debug=app.config["DEBUG"])
