let champCourriel = null;

function gererinfosCourriels(resultats){
    trouver = null;
    aComparer = champCourriel.val();

    for (let i = 0; i < resultats.length; i++) {
        if(resultats[i].courriel == aComparer){
            trouver = true;
            break;
        }
        else
        {
            trouver = false;
        }
    }
    if(trouver){
        champCourriel[0].classList.remove("border-green");
        champCourriel[0].classList.add("border-red");
    }
    else{
        champCourriel[0].classList.remove("border-red");
        champCourriel[0].classList.add("border-green");
    }
}

function initialisation() {
    champCourriel = $("#courriel");
    champCourriel.on("change", function(){
        $.ajax(
            {
                url : "/api/courriels",
                dataType : "json",
                success : gererinfosCourriels
            }
        )
    })
}

$(document).ready(initialisation)