let btn = null;

function confirmation(){
    alert("Étiquette ajoutée!");
    $("#titre").val("");
}

function ajouter_etiquette(){
//aller chercher titre
    let titre = $("#titre").val();
    const etiquette = {
        "titre": titre
    }

    $.ajax(
        {
            url     : "/etiquettes/creer_etiquette",
            "data"  : titre,
            error   : confirmation,
        }
    )
}


function initialisation() {
    btn = $("#bouton");
    btn.on('click', function(){
        ajouter_etiquette();
    });
}
$(document).ready(initialisation)