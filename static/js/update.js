const VideosMisAJour = $("#videosAccueil")
let nouvellesVideos= []
let etiquettes = []
let srcs = []

function gererRetourEtiquettes(resultats){
    let Maj = [];
    Maj.push(nouvellesVideos);
    Maj.push(etiquettes);
    Maj.push(srcs);
    $.ajax(
        {
            url : "/api/updateAccueil",
            data : Maj
        }
    )
}
function gererRetourSrcs(nouvellesVideos){
    for(i = 0; i < nouvellesVideos.length; i ++){
        etiquettes[i] = $.ajax(
            {
                url : "/api/etiquettesupdate",
                data : resultats,
                success : gererRetourEtiquettes,
            }
        )
    }
}
function gererRetourVideos(resultats){
    srcs = $.ajax(
        {
            url : "/api/srcsUpdate",
            data: resultats,
            success: gererRetourSrcs
        }
    )
}

function initialisation(){
    setInterval(function(){
        nouvellesVideos = $.ajax(
            {
                url : "/api/updateVideos",
                success : gererRetourVideos
            }
        )
    }, 10000)
}

$(document).ready(initialisation);