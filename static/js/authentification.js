const regexCourriel = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
let champCourriel = null;
let champMotPasse = null;
let divCourriel = null;
let message = null;
let emailValide = false;
let motPasseValide = false;
let messageAjoute = false;

function changement(){
    if (!regexCourriel.test(champCourriel.val().trim()) && !messageAjoute) 
    {
        emailValide = false;
    }
    else
    {
        emailValide = true;
    }
    if (champMotPasse.val().length <= 4 && !messageAjoute)
    {
        motPasseValide = false;
    }
    else
    {
        motPasseValide = true;
    }
    if(!emailValide || !motPasseValide)
    {
        $("#courriel")[0].classList.remove("border-green");
        $("#mdp")[0].classList.remove("border-green");
        $("#courriel")[0].classList.add("border-red");
        $("#mdp")[0].classList.add("border-red");
        
        divCourriel.append(message);
    }
    else
    {
        $("#courriel")[0].classList.remove("border-red");
        $("#mdp")[0].classList.remove("border-red");
        $("#courriel")[0].classList.add("border-green");
        $("#mdp")[0].classList.add("border-green");
        message.remove();
    }
}


function initialisation() {
    message = document.createElement("div");
    message.id = "message-erreur";
    message.classList = "text-danger";
    message.append(document.createTextNode("Courriel ou mot de passe invalide"));
    divCourriel = $("#div-form");
    champCourriel = $("#courriel");
    champMotPasse = $("#mdp");
    $("input").on('change', changement);
}
$(document).ready(initialisation)