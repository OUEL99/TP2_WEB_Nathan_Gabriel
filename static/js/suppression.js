let btnSupprimer = null;
let video = null;
let confirmation = null;
let btnConfirmer = null;
let videoId = null;


function success(){
    alert('réussi!');
}


function error(){
    alert('error');
}


function suppression(videoId){


    const id_video = {
        "id" : videoId
    }
    $.ajax(
        {
            url     : "/video/" + videoId + "/suppression",
            "data"  : id_video,
            success : success,
            error   : error
        }
    )
}



function confirmer_suppression() {
    video.remove();
    $("#main").append(confirmation);
    $("#btnConfirmer").on("click", function(){
        suppression(videoId);
    })
}


function initialisation() {
    video = $("#video");
    videoId = $("#btn-supprimer").val();
    messageVideoSupprime = document.createElement("h2");
    messageVideoSupprime.classList = "text-primary fw-bold"
    messageVideoSupprime.appendChild(document.createTextNode("Voulez vous vraiment supprimer ce video?"));

    btnConfirmer = document.createElement("a");
    btnConfirmer.id = "btnConfirmer";
    btnConfirmer.classList = "btn btn-outline-primary";
    btnConfirmer.appendChild(document.createTextNode("Supprimer"));

    confirmation = document.createElement("div");
    confirmation.classList = "text-center";
    confirmation.appendChild(messageVideoSupprime);
    confirmation.appendChild(btnConfirmer);

    btnSupprimer = $("#btn-supprimer");

    btnSupprimer.on("click", confirmer_suppression);

}

$(document).ready(initialisation);