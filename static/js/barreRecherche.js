let champRecherche = null;
let divSuggestions = null;
let recherches = null;


function gererRecherches() {
    divSuggestions.empty()
 
    divSuggestions[0].classList.add("afficher");
     
    const ul = document.createElement("ul")
    divSuggestions.append(ul)
    recherches = JSON.parse(window.localStorage.getItem("recherche"))
    for (let i=0; i< 5; i++) {
        const li = document.createElement("li");
        li.setAttribute("id", "li_suggest")
        var texte = document.createTextNode(JSON.stringify(recherches[i]))
        li.appendChild(texte);
        ul.append(li);
    }
 
      // Ajout d'un événement sur tout le document (la fenêtre)
      $(document).on("click", gererClicFenetre)
  }

function SauvegardeRecherche(){
    if(JSON.parse(window.localStorage.getItem("recherche")) != null){

        recherches = JSON.parse(window.localStorage.getItem("recherche"));
        window.localStorage.clear();
        recherches.unshift(champRecherche.val());
        window.localStorage.setItem("recherche", JSON.stringify(recherches))
        const li = document.getElementsByID("li_suggest")
        for(element in li){
            element.remove();
        }
    }
    else{
        recherches = [];
        recherches.unshift(champRecherche.val());
        window.localStorage.setItem("recherche", JSON.stringify(recherches));
    }
}

function initialisation(){
    divSuggestions = $("#div-suggestions")
    champRecherche = $("#mot-cle");
    champRecherche.on("click", gererRecherches);
    boutonRecherche = $("#boutonRecherche");
    boutonRecherche.on("click", SauvegardeRecherche);

}

$(document).ready(initialisation);