/**
 * Script pour la recherche
 */

"use strict";


// Référence vers l'objet servant à la communication avec le serveur.
let titres = null


/**
 * Appelée lors d'une erreur.
 */
function gererErreur(client, textStatus, errorThrown) {
    if (client.status == 0) {
        console.log("Requête annulée") // Donc tout est OK
    } else {
        
        console.error(`Erreur (code=${client.status}): ${textStatus}`)
        if (errorThrown != null) {
            console.error(errorThrown)
        }
        
    }
}


/**
 * Fonction appelée lorsque la réponse AJAX revient.
 */
function gererRetourRecherche(resultats) {
  divSuggestions.empty()
 
  divSuggestions[0].classList.add("afficher");
   
  const ul = document.createElement("ul")
  divSuggestions.append(ul)
  
  const autocomplete = JSON.parse(resultats)

  for (let i=0; i< 5; i++) {
    const li = document.createElement("li");
    li.setAttribute("id", "li_suggest")
    var texte = document.createTextNode(JSON.stringify(autocomplete[i]))
    li.appendChild(texte);
    ul.append(li);
  }
    titres = null
}



/**
 * Fonction appelée pour tenter de récupérer et d'afficher les informations
 * d'une personne avec AJAX.
 */
function rechercher() {
    

    let aChercher = $("#mot-cle").val().trim()

    

    if (titres != null) {
        // Annuler la requête précédente car on lancera une nouvelle requête
        // à chaque input et on ne veut plus le résultat de la requête précédente.
        titres.abort();
    }

    const parametres = {
        "mots-cles": aChercher
    }

    titres = $.ajax(
        {
            url     : "/api/autocompletion",
            "data"  : parametres,                   // Sont envoyés en GET si aucune méthode n'est spécifiée
            success :  gererRetourRecherche,
            error   : gererErreur
        }
    )
}


/**
 * Appelée lors de l'initialisation de la page
 */
function initialisation() {
    $("#mot-cle").on("input", rechercher);
}


$(document).ready(initialisation);

