
import re
from flask import Blueprint, session, render_template, request, redirect, abort, make_response, flash
from db import get_utilisateur, creer_connexion, ajouter_utilisateur, retirer_utilisateur
# from blueprints import Blueprint
from utilitaires import hacher_mdp, non_cache

compte = Blueprint('compte', __name__, template_folder='templates', url_prefix='/compte')


@compte.route('/listage_comptes', methods=['GET', 'POST'])
def listage():
    """Pour afficher les comptes"""
    if session is None or session["utilisateur"]["admin"] == 0:
        abort(403)
    comptes = None
    with creer_connexion() as connexion:
            with connexion.get_curseur() as curseur:
                curseur.execute(
                    "select * from compte_utilisateur", {

                    })
                comptes = curseur.fetchall()
    non_cache_listage(comptes)
    return render_template('listageComptes.jinja', comptes=comptes)


@compte.route('/authentifier', methods=['GET', 'POST'])
def authentifier():
    """Pour effectuer une authentification"""
    erreur = False
    courriel = request.form.get("courriel", default="")
    
    if (request.method == 'POST'):
        mdp = hacher_mdp(request.form.get("mdp"))

        with creer_connexion() as conn:
            utilisateur = get_utilisateur(
                conn,
                courriel
            )
            erreur = (not utilisateur)
            if(utilisateur["mot_de_passe"] != mdp):
                erreur = True
            if not erreur:
              #  session["admin"] = utilisateur.admin
                session["utilisateur"] = utilisateur
                return redirect('/', code=303)

    return render_template(
        'authentifier.jinja',
        courriel=courriel,
        erreur=erreur
    )

@compte.route('/creer', methods=['GET', 'POST'])
def creer():
    """Pour créer un compte"""
    #if session["admin"] == 0:
    #    abort(403)




    champs = {
        "courriel": {
            "valeur": request.form.get("courriel", default=""),
            "en_erreur": False
        },
        "mdp": {
            "en_erreur": False
        },
        "admin": {
            "en_erreur": False
        },
    }

    non_cache_creer(champs)
    if (request.method == 'POST'):
        mdp = request.form.get("mdp")
        admin = request.form.get("admin")

        if mdp != request.form.get("mdp2"):
            champs["mdp"]["en_erreur"] = True
        elif len(mdp) < 8:
            champs["mdp"]["en_erreur"] = True
        elif re.search("/[[:upper:]]+[[:lower:]]+\d/g", mdp) == False:
            champs["mdp"]["en_erreur"] = True
        else:
            mdp = hacher_mdp(mdp)
            with creer_connexion() as conn:
                utilisateur = {
                    "courriel": champs["courriel"]["valeur"],
                    "mot_de_passe": mdp,
                    "admin": admin,
                }

                ajouter_utilisateur(conn, utilisateur)
                flash("Votre compte a été créé.")
                return redirect('/', code=303)
    
    return render_template('creer.jinja', champs=champs)

@compte.route('/deconnecter', methods=['GET', 'POST'])
def deconnecter():
    session.pop("utilisateur", None)
    return redirect("/", code=303)

@compte.route('/supprimer', methods=['GET', 'POST'])
def supprimer():
    courriel = request.args.get("utilisateur")
    with creer_connexion() as conn:
        retirer_utilisateur(conn, courriel)
    

    return redirect("/compte/listage_comptes", code=303)

@compte.errorhandler(403)
def not_connected_error(_):
    '''Pour les erreurs 403'''
    return render_template(
        'erreur403.jinja',
        message="Vous n'êtes pas connecté à un compte Administrateur."
    ), 403

@compte.route("/non_cache_creer")
def non_cache_creer(champs):
    """Page non cache de la page creer"""
    lesChamps = champs
    reponse = make_response(render_template('creer.jinja', champs=lesChamps))
    reponse.cache_control.max_age = 0
    reponse.cache_control.no_store = True
    return reponse

@compte.route("/non_cache_listage")
def non_cache_listage(comptes):
    """Page non cache de la page creer"""
    lesComptes = comptes
    reponse = make_response(render_template('listageComptes.jinja', comptes=lesComptes))
    reponse.cache_control.max_age = 0
    reponse.cache_control.no_store = True
    return reponse
