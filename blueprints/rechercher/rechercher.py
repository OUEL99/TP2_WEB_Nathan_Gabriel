from flask import Blueprint, render_template, abort, request, jsonify
from jinja2 import TemplateNotFound
from db import creer_connexion
import config

rechercher = Blueprint('rechercher', __name__,
                               template_folder='templates')

@rechercher.route('/rechercher')
def recherche_api():
    """
    Returns:
        Page correspondant à la recherche
    """
    videos = None
    mot_cle = request.args.get("mot-cle", default="").strip()
    srcs = []
    uploadF = 'static/video/'
    with creer_connexion() as connexion:
        with connexion.get_curseur() as curseur:
            curseur.execute(
                "select * from videos WHERE titre LIKE %(mot_cle)s;", {
                    'mot_cle': f"%{mot_cle}%"
                })
            videos = curseur.fetchall()
    liste_videos = []
    for le_video in videos:
        if le_video["visibilite"] == 0:
            srcs.append(uploadF + le_video['nom_fichier'])
    jsonVideos = jsonify(videos, srcs)
    # return jsonify(videos, srcs)
    return render_template("rechercher.jinja", videos=videos, srcs=srcs, mot_cle=mot_cle)

# @rechercher.route('/rechercher')
# def recherche():
#     return render_template("rechercher.jinja", videos=videos, srcs=srcs, mot_cle=mot_cle, jsonVideos=jsonVideos)