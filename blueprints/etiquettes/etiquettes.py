from flask import render_template, session, abort, Blueprint, request, flash, jsonify, redirect, url_for
from db import creer_connexion
from utilitaires import get_toggle_etiquettes, get_video_par_etiquette, get_etiquettes, get_valeur_max_etiquette

etiquette = Blueprint('etiquettes', __name__,
                               template_folder='templates', url_prefix="/etiquettes")
    
@etiquette.route("/", methods=["GET", "POST"])
def etiquettes():
    """Page d'étiquettes"""
    if session["utilisateur"] == None:
        abort(401)
    etiquettes = get_toggle_etiquettes()

    return render_template(
        "etiquettes.jinja", etiquettes=etiquettes
    )

@etiquette.route("/creer_etiquette", methods=["GET", "POST"])
def creer():
    """
    Returns:
        Formulaire d'ajout
    """
    if session["utilisateur"] == None:
        abort(401)

    titre = {}
    maxVal = get_valeur_max_etiquette()[0]['MAX(id)']

    if request.method != "POST":
        return render_template(
            "creer_etiquette.jinja",
            titre=titre
        )
    else:
        maxVal += 1
        titre = request.form.get("titre", default="").strip()
        with creer_connexion() as connexion:
            with connexion.get_curseur() as curseur:
                curseur.execute(
                    "INSERT INTO `etiquettes`(`id`, `libelle`) VALUES(%(maxVal)s, %(titre)s);",{
                        'maxVal': maxVal,
                        'titre': titre
                    }
                )
    return('', 204)

@etiquette.route("/<int:etiquette>")
def etiquette_video(etiquette):
    videos = {}
    videos = get_video_par_etiquette(etiquette)
    return render_template(
        "videos_etiquette.jinja", videos=videos
    )

# @etiquettes.errorhandler(401)
# def not_connected_error(_):
#     '''Pour les erreurs 401'''
#     return render_template(
#         'erreur401.jinja',
#         message="Vous n'êtes pas connecté."
#     ), 401
# @etiquette.route("/nouvelle_etiquette", methods=["GET", "POST"])
# def nouvelle_etiquette():
#     """Page d'étiquettes"""
#     if session["utilisateur"] == None:
#         abort(401)
#     etiquettes = get_toggle_etiquettes()
#     titre = {}
#     maxVal = get_valeur_max_etiquette()[0]['MAX(id)']
#     maxVal += 1
#     titre = request.form.get("titre", default="").strip()
#     with creer_connexion() as connexion:
#         with connexion.get_curseur() as curseur:
#             curseur.execute(
#                 "INSERT INTO `etiquettes`(`id`, `libelle`) VALUES(%(maxVal)s, %(titre)s);",{
#                     'maxVal': maxVal,
#                     'titre': titre
#                 }
#             )
#     return render_template(
#         "nouvelle_etiquette.jinja", etiquettes=etiquettes
#     )
