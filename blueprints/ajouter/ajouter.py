from flask import Blueprint, render_template, request, redirect, url_for, session, abort, current_app, flash
from jinja2 import TemplateNotFound
from db import creer_connexion
from werkzeug.utils import secure_filename
from utilitaires import get_toggle_etiquettes
import os


def ajouter_v_libelles(id_video, etiquettes_choisies):
    """Ajouter le lien entre les videos et les etiquettes"""
    for etiquette in etiquettes_choisies:
        with creer_connexion() as connexion:
            with connexion.get_curseur() as curseur:
                curseur.execute(
                    "INSERT INTO `videos_libelles`(`id_video`, `id_etiquette`) VALUES (%(id_video)s, %(id_etiquette)s)", {
                        'id_video': id_video,
                        'id_etiquette': etiquette
                    })


def get_id_nouveau_video():
    """test"""
    with creer_connexion() as connexion:
        with connexion.get_curseur() as curseur:
            curseur.execute(
                'select MAX(id) from videos')
            id_nouv = curseur.fetchone()
    int_id = int(id_nouv['MAX(id)']) + 1
    return int_id


ajouter = Blueprint('ajouter', __name__, template_folder='templates')


@ajouter.route('/ajouter', methods=["GET", "POST"])
def render():
    """
    Returns:
        Formulaire d'ajout
    """
    if session["utilisateur"] == None:
        abort(401)

    titre = {}
    description = {}
    nom_fichier = {}
    les_etiquettes = get_toggle_etiquettes()
    erreur = False
    if request.method != "POST":

        return render_template(
            "ajouter.jinja",
            titre=titre, 
            description=description,
            hyperlien=nom_fichier,
            etiquettes=les_etiquettes,
        )
    else:
        la_visibilite = request.form.get("visible")
        titre = request.form.get("titre", default="").strip()
        description = request.form.get(
            "description", default="").strip()
        fichier = request.files['fichier']
        nom_fichier = secure_filename(fichier.filename)
        fichier_ext = os.path.splitext(nom_fichier)[1]
        msg = ""
        etiquettes_choisies = request.form.getlist("etiquette")
        if fichier.seek(0, os.SEEK_END):
            fichier_length = fichier.tell()
            if fichier_length > current_app.config['MAX_CONTENT_LENGTH']:
                msg = "Le fichier est trop grand!"
                erreur = True
            elif fichier_ext not in current_app.config['UPLOAD_EXTENSIONS']:
                msg = "Le type de fichier n'est pas supporté!"
                erreur = True
            fichier.seek(0, 0)
        if erreur:
            return ajouter.bad_request(msg)

        src = current_app.config['UPLOAD_FOLDER'] + nom_fichier
        fichier.save(src)
        id_video = get_id_nouveau_video()
        with creer_connexion() as connexion:
            with connexion.get_curseur() as curseur:
                curseur.execute(
                    "INSERT INTO `videos`(`titre`, `description`, `nom_fichier`, `visibilite`) VALUES(%(le_titre)s, %(la_description)s, %(nom_fichier)s, %(visibilite)s);", {
                        'le_titre': titre,
                        'la_description': description,
                        'nom_fichier': nom_fichier,
                        'visibilite': la_visibilite
                    })

        flash("Vidéo ajouté!")
        ajouter_v_libelles(id_video, etiquettes_choisies)
        return redirect(url_for('video.render', id_video=id_video))


@ajouter.errorhandler(401)
def not_connected_error(_):
    '''Pour les erreurs 401'''
    return render_template(
        'erreur401.jinja',
        message="Vous n'êtes pas connecté."
    ), 401
