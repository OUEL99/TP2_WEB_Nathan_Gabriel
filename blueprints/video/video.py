from flask import Blueprint, render_template, request, flash, url_for, redirect
from jinja2 import TemplateNotFound
from db import creer_connexion
from utilitaires import get_etiquettes, get_commentaires, laisser_commentaire
import config

video = Blueprint('video', __name__,
                               template_folder='templates', url_prefix="/video")
                               
@video.route('/<int:id_video>', methods=['GET', 'POST'])
def render(id_video:int):

    if request.method == "POST":
        user=request.form.get("user")
        commentaire=request.form.get("commentaire")
        laisser_commentaire(commentaire, id_video, user)
        flash("Commentaire ajouté!")
    with creer_connexion() as connexion:
        with connexion.get_curseur() as curseur:
            curseur.execute(
                "select id, titre, description, nom_fichier from videos WHERE `id` = %(id_video)s;", {
                    'id_video': id_video
                })
            video = curseur.fetchone()
            src = config.DevelopmentConfig.UPLOAD_FOLDER + video['nom_fichier']
    etiquettes = get_etiquettes(id_video)
    commentaires = get_commentaires(id_video)
    return render_template(
        "video.jinja", video=video, src=src, etiquettes=etiquettes,commentaires=commentaires, description=video['description'], id=id_video
    )

@video.route('/<int:id_video>/suppression')
def suppression(id_video:int):
    with creer_connexion() as connexion:
        with connexion.get_curseur() as curseur:
            curseur.execute(
                "DELETE FROM videos WHERE `id` = %(id_video)s;",{
                    'id_video': id_video
                }
            )
    return(' ', 204)