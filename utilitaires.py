"""
Fonctions utilitaires
"""

import hashlib

from flask import abort, session, make_response
from flask.templating import render_template
import db

def get_toggle_etiquettes():
    """Get etiquettes"""
    with db.creer_connexion() as connexion:
        with connexion.get_curseur() as curseur:
            curseur.execute(
                'select id, libelle from etiquettes')
            les_etiquettes = curseur.fetchall()
    return les_etiquettes

def get_utilisateur_authentifié_or_die():
    """Retourne l'utilisateur authentifié ou faire un abort(code)"""
    if "utilisateur" not in session:
        abort(401)

    return session["utilisateur"]

def get_utilisateur_or_die(conn, identifiant):
    """Retourne le profil d'un utilisateur existant ou faire un abort(code)"""
    profile = db.get_utilisateur(conn, identifiant)
    if profile is None:
        abort(404)
    return profile
    
def hacher_mdp(mdp_en_clair):
    """Prend un mot de passe en clair et lui applique une fonction de hachage"""
    return hashlib.sha512(mdp_en_clair.encode('utf-8')).hexdigest()

def get_id_etiquettes(id_video):
    """Aller chercher les étiquettes pour un video"""
    with db.creer_connexion() as connexion:
        with connexion.get_curseur() as curseur:
            curseur.execute(
                "select id_etiquette from videos_libelles WHERE `id_video` = %(id_video)s;", {
                    'id_video': id_video
                })
            ids = curseur.fetchall()
    id_etiquettes = []
    for ids_courants in ids:
        id_etiquettes.append(ids_courants['id_etiquette'])
    return id_etiquettes;

def get_etiquettes(id_video):
    """Aller chercher les étiquettes pour un video"""
    id_etiquettes = get_id_etiquettes(id_video)
    etiquettes = []
    for id_etiquette in id_etiquettes:
        with db.creer_connexion() as connexion:
            with connexion.get_curseur() as curseur:
                curseur.execute(
                    "select libelle from etiquettes WHERE `id` = %(id_etiquette)s;", {
                        'id_etiquette': id_etiquette
                    })
                etiquette = []
                etiquette.append(curseur.fetchone()['libelle'])
                etiquette.append(id_etiquette)
                etiquettes.append(etiquette)
    return etiquettes

def get_valeur_max_etiquette():
    with db.creer_connexion() as connexion:
        with connexion.get_curseur() as curseur:
            curseur.execute(
                "SELECT MAX(id) FROM etiquettes;"
            )
            return curseur.fetchall();


def supprimerCompte(conn, courriel):
    with conn.get_curseur() as curseur:
        curseur.execute(
            "DELETE FROM compte_utilisateur WHERE 'courriel' = %(Courriel)s;", {
                "Courriel": courriel
            })
    #Manque le Flash pour la confirmation

def get_commentaires(video):
    with db.creer_connexion() as connexion:
        with connexion.get_curseur() as curseur:
            curseur.execute(
                "SELECT commentaire, user FROM commentaires WHERE video = %(video)s;",{
                    "video": video
                }
            )
            return curseur.fetchall()

def get_video_par_etiquette(etiquette):
    with db.creer_connexion() as connexion:
        with connexion.get_curseur() as curseur:
            curseur.execute(
                "select * from videos left join videos_libelles ON videos.id = videos_libelles.id_video where videos_libelles.id_etiquette LIKE '%%(etiquette)s%'",
                {
                    "etiquette": etiquette
                }
            )
            return curseur.fetchall()

def non_cache(page, champs):
    """ Page non cache """
    reponse = make_response(render_template(page, champs))
    reponse.cache_control.max_age = 0
    reponse.cache_control.no_store = True
    return reponse

def laisser_commentaire(commentaire, id_video, user):
    with db.creer_connexion() as connexion:
        with connexion.get_curseur() as curseur:
            curseur.execute("INSERT INTO commentaires(video, commentaire, user) VALUES(%(id_video)s, %(commentaire)s, %(user)s);", 
            {
                'id_video': id_video,
                'commentaire': commentaire,
                'user': user
            }
        )
